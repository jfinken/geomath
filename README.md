# Geospatial mathematical functions for Go

## Supported functions:

 * Haversine distance
 * Midpoint
 * Destination
