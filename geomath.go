package geomath

import (
	"math"
)

//EarthRadiusMean is the mean radius of the earth
const EarthRadiusMean float64 = 6371009.0
const EarthRadiusMeridional float64 = 6367449.1
const EarthRadiusPolar float64 = 6356752.3

//EarthRadiusEquitorial is the radius of the earth at the equator
const EarthRadiusEquitorial float64 = 6378137.0

//Location encapsulates latitude, longitude and altitude
type Location struct {
	Lat float64
	Lon float64
	Alt float64
}

func toRad(deg float64) float64 {
	return math.Pi * deg / 180.0
}
func toDeg(rad float64) float64 {
	return rad / math.Pi * 180.0
}

// EarthRadius returns the earth's radius at the specified latitude
func EarthRadius(lat float64) float64 {
	a := EarthRadiusEquitorial
	b := EarthRadiusPolar
	rlat := toRad(lat)
	coslat := math.Cos(rlat)
	sinlat := math.Sin(rlat)
	num := math.Pow(a*a*coslat, 2) + math.Pow(b*b*sinlat, 2)
	denom := math.Pow(a*coslat, 2) + math.Pow(b*sinlat, 2)
	return math.Sqrt(num / denom)
}

// HaversineDistance returns the "as the crow flies" distance between the two
// specified locations using the Haversine formula. The function uses the
// Earth's average radius.
func HaversineDistance(a, b *Location) float64 {

	deltaLat := ((math.Pi / 180.0) * a.Lat) - ((math.Pi / 180.0) * b.Lat)
	deltaLon := ((math.Pi / 180.0) * a.Lon) - ((math.Pi / 180.0) * b.Lon)

	aval := math.Pow(math.Sin(deltaLat/2.0), 2) + math.Cos((math.Pi/180.0)*b.Lat)*
		math.Cos((math.Pi/180.0)*a.Lat)*math.Pow(math.Sin(deltaLon/2.0), 2)

	//the arctangent of y / x given y and x, but with a range of
	cval := 2 * math.Atan2(math.Sqrt(aval), math.Sqrt(1-aval))

	//uses the average radius of the Earth
	distance := EarthRadiusMean * cval
	// account for Altitude if available
	if a.Alt != 0 && b.Alt != 0 {
		deltaAlt := math.Abs(a.Alt - b.Alt)
		distance = math.Sqrt(deltaAlt*deltaAlt + math.Pow(distance, 2))
	}
	return distance
}

//Midpoint returns the midpoint Location between two input Locations
// http://www.movable-type.co.uk/scripts/latlong.html
func Midpoint(a, b *Location) *Location {
	alat := toRad(a.Lat)
	alng := toRad(a.Lon)
	blat := toRad(b.Lat)
	blng := toRad(b.Lon)
	deltaLon := math.Abs(blng - alng)
	px := math.Cos(blat) * math.Cos(deltaLon)
	py := math.Cos(blat) * math.Sin(deltaLon)
	clat := math.Atan2(math.Sin(alat)+math.Sin(blat),
		math.Sqrt((math.Cos(alat)+px)*(math.Cos(alat)+px)+py*py))
	clng := alng + math.Atan2(py, math.Cos(alat)+px)

	return &Location{Lat: toDeg(clat), Lon: toDeg(clng), Alt: ((a.Alt + b.Alt) / 2.0)}
}

// Destination returns a point given a start point, a distance in meters from the
// start point, and a bearing (clockwise from North in degrees).
// http://www.movable-type.co.uk/scripts/latlong.html
func Destination(start *Location, bearing, dist float64) *Location {

	angDist := dist / EarthRadius(start.Lat)
	slat := toRad(start.Lat)
	brng := toRad(bearing)
	lat := math.Asin(math.Sin(slat)*math.Cos(angDist) +
		math.Cos(slat)*math.Sin(angDist)*math.Cos(brng))

	lon := toRad(start.Lon) +
		math.Atan2(math.Sin(brng)*math.Sin(angDist)*math.Cos(slat),
			math.Cos(angDist)-math.Sin(slat)*math.Sin(lat))

	// For final bearing, take the initial bearing from the end point to the
	// start point and reverse it (using θ = (θ+180) % 360)

	return &Location{Lat: toDeg(lat), Lon: toDeg(lon), Alt: start.Alt}
}
