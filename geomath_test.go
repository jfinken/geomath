package geomath

import (
	"math"
	"testing"
)

func TestEarthRadius(t *testing.T) {
	lat := 41.931449

	r := EarthRadius(lat)
	t.Errorf("For lat: %f, got radius: %f", lat, r)
}

func TestHaversineDistance(t *testing.T) {

	expectedWithAlt := 2836.90
	expectedNoAlt := 2836.86

	lowerLeft := &Location{Lat: 41.931449, Lon: -87.745485, Alt: 31.5}
	upperRight := &Location{Lat: 41.953155, Lon: -87.727461, Alt: 16.2}

	// note truncation
	distance := Round(HaversineDistance(lowerLeft, upperRight), 2)

	if distance != expectedWithAlt {
		t.Errorf("geomath.HaversineDistance error!  Got %f, Expected %f",
			distance, expectedWithAlt)
	}

	lowerLeft = &Location{Lat: 41.931449, Lon: -87.745485}
	upperRight = &Location{Lat: 41.953155, Lon: -87.727461}

	// note truncation
	distance = Round(HaversineDistance(lowerLeft, upperRight), 2)

	if distance != expectedNoAlt {
		t.Errorf("geomath.HaversineDistance error!  Got %f, Expected %f",
			distance, expectedNoAlt)
	}

}
func TestMidpoint(t *testing.T) {

	lowerLeft := &Location{Lat: 41.931449, Lon: -87.745485, Alt: 31.5}
	upperRight := &Location{Lat: 41.953155, Lon: -87.727461, Alt: 16.2}
	expected := &Location{Lat: 41.9423200, Lon: -87.7364750, Alt: 23.85}

	mp := Midpoint(lowerLeft, upperRight)
	// Note truncation
	mp = &Location{Lat: Round(mp.Lat, 6), Lon: Round(mp.Lon, 6), Alt: Round(mp.Alt, 2)}

	if mp.Lat != expected.Lat && mp.Lon != expected.Lon && mp.Alt != expected.Alt {
		t.Errorf("geomath.Midpoint error!  Got %f, Expected %f",
			mp, expected)
	}
}
func TestDestination(t *testing.T) {
	loc := &Location{Lat: 41.931449, Lon: -87.745485, Alt: 31.5}
	expected := &Location{Lat: 41.937963, Lon: -87.736728, Alt: 31.5}
	bearing := 45.0
	dist := 1024.0

	calc := Destination(loc, bearing, dist)

	if calc.Lat != expected.Lat && calc.Lon != expected.Lon && calc.Alt != expected.Alt {
		t.Errorf("geomath.Destination error!  Got %f, Expected %f",
			calc, expected)
	}
}
func Round(f float64, places int) float64 {
	shift := math.Pow(10, float64(places))
	return math.Floor((f*shift)+.5) / shift
}
